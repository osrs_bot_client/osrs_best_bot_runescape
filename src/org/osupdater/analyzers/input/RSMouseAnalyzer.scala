package org.osupdater.analyzers.input

import org.osupdater.Analyzer
import org.osupdater.util.Util

class RSMouseAnalyzer (name: String)
  extends Analyzer (name) {

  def run: Boolean = {
    List(classNode.interfaces.toArray(new Array[String](0)) : _*).contains("java/awt/event/MouseListener") &&
    Util.listFields(classNode).exists((p) => p.desc == "L" + className + ";" && add("mouse", List(p.desc, p.name)))
  }
}

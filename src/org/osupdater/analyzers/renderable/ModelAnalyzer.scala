package org.osupdater.analyzers.renderable

import java.util

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.deob.cfg.Block
import org.osupdater.deob.cfg.heuristics.{SimpleHeuristic, Heuristic, BranchDecision}
import org.osupdater.util.Util
import org.osupdater.{Analyzer, Updater}

class ModelAnalyzer(name: String)
  extends Analyzer (name) {

  val var8_non_null = new Heuristic {
    override def run(b: Block, blocks: util.Map[Integer, Block]): BranchDecision = {
      if (Util.findPattern(Util.listInstructions(b), List((p: AbstractInsnNode) => p.getOpcode == Opcodes.ALOAD && p.asInstanceOf[VarInsnNode].`var` == 8, Opcodes.IFNULL)))
        BranchDecision.FALL
      else
        BranchDecision.INCONCLUSIVE
    }
  }

  val indexcount_jmp = new Heuristic {
    override def run(b: Block, blocks: util.Map[Integer, Block]): BranchDecision = {
      if (Util.findPattern(Util.listInstructions(b),
        List(
          (p: AbstractInsnNode) => p.getOpcode == Opcodes.ALOAD && p.asInstanceOf[VarInsnNode].`var` == 8,
          Opcodes.GETFIELD,
          (p: AbstractInsnNode) => p.isInstanceOf[JumpInsnNode]
        )
      ))
        BranchDecision.FALL
      else
        BranchDecision.INCONCLUSIVE
    }
  }

  // Model is the only subclass of Renderable that overrides renderAtPoint from Renderable.
  def findRenderAtPoint: Boolean = {
    val renderableMethods = Util.listMethods(Updater.classes(Updater.classMap("Renderable"))).filter((m) => Util.checkException(m)).toSet[MethodNode].map((m) => m.name + ":" + m.desc)
    methods.exists((m) => m.desc == "(IIIIIIIII)V" && renderableMethods.contains(m.name + ":" + m.desc))
  }

  /*

  We can conveniently grab the vertex xyz arrays and vertex count from the method that translates them by a vector, e.g.

  public void bg(int var1, int var2, int var3) {
    for(int var4 = 0; var4 < this.w; ++var4) {
      this.c[var4] += var1;
      this.x[var4] += var2;
      this.f[var4] += var3;
    }
    this.ah = 0;
  }

  Here w, c, x, f = count, x, y, z - the obfuscator doesn't change the order of method arguments,
  nor the order of the statements, so for now I will simply map the first getfield to count, the second to x,
  the third to y and the fourth to z. If this ever changes, I'll just find where they do trig with these values
  as it gives x/y/z away.

  This effectively turns into zipping all the getfields with ("vertexCount", "verticesX", "verticesY", "verticesZ").

  ---

  The index xyz arrays and index count are easiest to retrieve from the constructor with the signature ([LModel;I)V,

    for(var9 = 0; var9 < var8.t; ++var9) {
      this.n[this.t] = var8.n[var9] + this.w;
      this.p[this.t] = var8.p[var9] + this.w;
      this.a[this.t] = var8.a[var9] + this.w;

      where t, n, p, a = count, x, y, z

  It is the first loop of the method that goes up to a field rather than a local variable, which will look like

    aload 8
    getfield ck/t I
    if_icmpge 160

  So, we can go through all the instructions, and everytime we hit a JumpInsnNode, check if it's preceeded by
  a getfield. If so, pick up the var name which will be our index count, next 3 different getfield "[I" s will be
  x/y/z.


  */

  def grabIndices(instructions: List[AbstractInsnNode], names: List[String]): Boolean = {
    instructions match {
      case (hd:FieldInsnNode) :: tail if results.values.forall(_ != hd.name) =>
        add(names.head, List(hd.desc, hd.name))
        // If there are no more vars to find, return true
        names.tail.length == 0 || grabIndices(tail, names.tail)
      case hd :: tail => grabIndices(tail, names)
    }
  }

  /*
    Find something like
      aload 8
      getfield ck/t I
      if_icmpge
  */
  def findLoop(instructions: List[AbstractInsnNode]): Boolean = {
    instructions match {
      case (aload: VarInsnNode) :: (get: FieldInsnNode) :: (jmp: JumpInsnNode) :: tail if get.desc == "I" && aload.`var` > 0 =>
        add("indexCount", List(get.desc, get.name))
        grabIndices(tail.filter((i) => i.getOpcode == Opcodes.GETFIELD && i.asInstanceOf[FieldInsnNode].desc == "[I"), List("indicesX", "indicesY", "indicesZ"))
      case hd :: tl => findLoop(tl)
      case _ => false
    }
  }

  def run: Boolean = {
    classNode.superName == Updater.classMap("Renderable") && findRenderAtPoint &&
      methods.filter((m) => m.desc == "(III)V" && !Util.existsInstruction(m, Opcodes.SIPUSH)).exists(
        (m) => {
          (Util.listInstructions(m, className).filter(_.getOpcode == Opcodes.GETFIELD), List("vertexCount", "verticesX", "verticesY", "verticesZ"))
            .zipped.map{case (x: FieldInsnNode, y) => add(y, List(x.desc, x.name))}
          true // To keep the expression intact
        }
      ) &&
      methods.filter((m) => m.desc == "([L" + className + ";I)V" && m.name == "<init>").exists(
        (m) => findLoop(Util.listInstructions(m, className, var8_non_null, indexcount_jmp))
      )
  }

}

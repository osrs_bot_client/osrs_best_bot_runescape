package org.osupdater.analyzers

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.{MethodNode, MultiANewArrayInsnNode, TypeInsnNode}
import org.osupdater.Analyzer
import org.osupdater.util.Util

class RegionAnalyzer(name: String)
  extends Analyzer(name) {

  val objCreation = "{\"name\": \"%s\", \"sig\": \"%s\", \"uid\": 12, \"conf\": 13, \"x\": 2, \"y\": 3}"
  val objDeletion = "{\"name\": \"%s\", \"sig\": \"%s\", \"obj\": 1}"

  // Find the method in the Region class that creates a new "InteractableObject",
  def hookObjectCreation(mn: MethodNode): Boolean = {
    mn.desc.matches("\\(IIIIIIIIL..;IZI.\\)Z") && add("createObject", String.format(objCreation, mn.name, mn.desc))
  }

  def hookObjectDeletion(mn: MethodNode): Boolean = {
    mn.desc.matches("\\(L..;\\)V") && add("deleteObject", String.format(objDeletion, mn.name, mn.desc))
  }

  def run: Boolean = {
    methods.exists((p) => p.name == "<clinit>" &&
      Util.listInstructions(p, className).exists((p) =>
        p.getOpcode == Opcodes.MULTIANEWARRAY &&
        p.asInstanceOf[MultiANewArrayInsnNode].desc == "[[[[Z")) &&
    methods.exists((p) => hookObjectCreation(p)) &&
    methods.exists((p) => hookObjectDeletion(p))
  }

}

package org.osupdater.analyzers.node

import java.lang.reflect.Modifier

import org.objectweb.asm.tree.FieldNode
import org.osupdater.Analyzer
import org.osupdater.util.Util

// The Node class is identifiable by having 2 Node variables (prev, next) and a value variable that is a long.
class NodeAnalyzer(name: String)
  extends Analyzer (name){

  def scanFields(fields: List[FieldNode], desc: String, selfs: Int, longs: Int): (Int, Int) = {
    fields match {
      case hd :: tail =>
        hd.desc match {
          case `desc` =>
            if (Modifier.isPublic(hd.access)) {
              add("prev", List(hd.desc, hd.name))
            } else
              add("next", List(hd.desc, hd.name))
            scanFields(tail, desc, selfs + 1, longs)
          case "J" =>
            add("value", List(hd.desc, hd.name))
            scanFields(tail, desc, selfs, longs + 1)
          case _ => scanFields(tail, desc, selfs, longs)
        }
      case _ => (selfs, longs)
    }
  }

  def run: Boolean = {
    classNode.fields.size() == 3 && scanFields(Util.listFields(classNode), "L" + className + ";", 0, 0) == (2,1)
  }

}

package org.osupdater.analyzers.definitions

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.{Updater, Analyzer}
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer

class PlayerDefAnalyzer (name: String)
  extends Analyzer (name) {

  def checkEquipmentArray(list: List[AbstractInsnNode]): Boolean = {
    list match {
      case (a : IntInsnNode) :: (b : IntInsnNode) :: tl
        if a.operand == 6 && b.operand == 10 => true
      case Nil => false
      case _ => checkEquipmentArray(list.tail)
    }
  }

  def getNPCId(list: List[AbstractInsnNode]): Boolean = {
    list match {
      case hd :: tl =>
        if (hd.isInstanceOf[JumpInsnNode]) {
          val expr = ExpressionTracer.trace(hd)
          expr.exists((p) => p.getOpcode == Opcodes.ICONST_M1) && {
            val f = expr.find((p) => p.getOpcode == Opcodes.GETFIELD).get.asInstanceOf[FieldInsnNode]
            add("npcID", List(f.desc, f.name, expr.find((p) => p.getOpcode == Opcodes.LDC).get.asInstanceOf[LdcInsnNode].cst.toString))
          }
        } else
          getNPCId(tl)
      case Nil => false
    }
  }


  def run: Boolean = {
    methods.exists((p) => p.name == "<clinit>" && checkEquipmentArray(Util.listInstructions(p, className))) &&
      methods.filter((p) => p.desc.endsWith("L" + Updater.classMap("Model") + ";") && Util.checkException(p)).exists((p) =>
        getNPCId(Util.listInstructions(p, className)))
  }

}

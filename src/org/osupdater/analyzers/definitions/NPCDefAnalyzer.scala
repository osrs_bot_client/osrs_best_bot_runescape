package org.osupdater.analyzers.definitions

import java.lang.reflect.Modifier

import org.objectweb.asm.Type
import org.objectweb.asm.tree.MethodNode
import org.osupdater.util.Util
import org.osupdater.{Updater, Analyzer}

// First check if the class extends CacheNode. Then we look for this method:
//   public final ck w(af var1, int var2, af var3, int var4, int var5)
class NPCDefAnalyzer (name: String)
  extends Analyzer (name) {

  def checkDescriptor(m: MethodNode): Boolean = {
    val args = Type.getArgumentTypes(m.desc).map((t) => t.getDescriptor).toList
    args match {
      case a :: b :: c :: d :: e if a.startsWith("L") && a == c && b == d && e.size == 1 => true
      case _ => false
    }
  }

  def getChildDef(m: MethodNode): Boolean = {
    Modifier.isPublic(m.access) &&
      !Modifier.isStatic(m.access) &&
      m.desc.endsWith(")L" + className + ";") &&
      Type.getArgumentTypes(m.desc).length == 1 && {
      add("getChildDefinition", List(m.desc, m.name, Util.getExceptionParam(Util.listInstructions(m, className), 1)))
    }
  }

  def run: Boolean = {
    classNode.superName == Updater.classMap("CacheNode") &&
      methods.exists((m) => Modifier.isPublic(m.access) && checkDescriptor(m)) && {
      Util.listFields(classNode).foreach((f) => f.desc match {
        case "Ljava/lang/String;" => add("name", List(f.desc, f.name))
        case "[Ljava/lang/String;" => add("actions", List(f.desc, f.name))
        case _ =>
      })
      true
    } && methods.exists((m) => getChildDef(m))
  }
}

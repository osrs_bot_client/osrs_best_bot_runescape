package org.osupdater.util

import org.objectweb.asm.Opcodes._
import org.objectweb.asm.tree._
import org.osupdater.Updater

/**
 * Created by Juuso on 3/30/2015.
 */
object ByteLocations {
  var lengths: Array[Int] = new Array[Int](0xFF)
  lengths(AALOAD) = 1
  lengths(AASTORE) = 1
  lengths(ACONST_NULL) = 1
  lengths(ALOAD) = 2
  lengths(ANEWARRAY) = 3
  lengths(ARETURN) = 1
  lengths(ARRAYLENGTH) = 1
  lengths(ASTORE) = 2
  lengths(ATHROW) = 1
  lengths(BALOAD) = 1
  lengths(BASTORE) = 1
  lengths(BIPUSH) = 2
  lengths(CALOAD) = 1
  lengths(CASTORE) = 1
  lengths(CHECKCAST) = 3
  lengths(D2F) = 1
  lengths(D2I) = 1
  lengths(D2L) = 1
  lengths(DADD) = 1
  lengths(DALOAD) = 1
  lengths(DASTORE) = 1
  lengths(DCMPG) = 1
  lengths(DCMPL) = 1
  lengths(DCONST_0) = 1
  lengths(DCONST_1) = 1
  lengths(DDIV) = 1
  lengths(DLOAD) = 2
  lengths(DMUL) = 1
  lengths(DNEG) = 1
  lengths(DREM) = 1
  lengths(DRETURN) = 1
  lengths(DSTORE) = 2
  lengths(DSUB) = 1
  lengths(DUP) = 1
  lengths(DUP_X1) = 1
  lengths(DUP_X2) = 1
  lengths(DUP2) = 1
  lengths(DUP2_X1) = 1
  lengths(DUP2_X2) = 1
  lengths(F2D) = 1
  lengths(F2I) = 1
  lengths(F2L) = 1
  lengths(FADD) = 1
  lengths(FALOAD) = 1
  lengths(FASTORE) = 1
  lengths(FCMPG) = 1
  lengths(FCMPL) = 1
  lengths(FCONST_0) = 1
  lengths(FCONST_1) = 1
  lengths(FCONST_2) = 1
  lengths(FDIV) = 1
  lengths(FLOAD) = 2
  lengths(FMUL) = 1
  lengths(FNEG) = 1
  lengths(FREM) = 1
  lengths(FRETURN) = 1
  lengths(FSTORE) = 2
  lengths(FSUB) = 1
  lengths(GETFIELD) = 3
  lengths(GETSTATIC) = 3
  lengths(GOTO) = 3
  lengths(I2B) = 1
  lengths(I2C) = 1
  lengths(I2D) = 1
  lengths(I2F) = 1
  lengths(I2L) = 1
  lengths(I2S) = 1
  lengths(IADD) = 1
  lengths(IALOAD) = 1
  lengths(IAND) = 1
  lengths(IASTORE) = 1
  lengths(ICONST_M1) = 1
  lengths(ICONST_0) = 1
  lengths(ICONST_1) = 1
  lengths(ICONST_2) = 1
  lengths(ICONST_3) = 1
  lengths(ICONST_4) = 1
  lengths(ICONST_5) = 1
  lengths(IDIV) = 1
  lengths(IF_ACMPEQ) = 3
  lengths(IF_ACMPNE) = 3
  lengths(IF_ICMPEQ) = 3
  lengths(IF_ICMPGE) = 3
  lengths(IF_ICMPGT) = 3
  lengths(IF_ICMPLE) = 3
  lengths(IF_ICMPLT) = 3
  lengths(IF_ICMPNE) = 3
  lengths(IFEQ) = 3
  lengths(IFGE) = 3
  lengths(IFGT) = 3
  lengths(IFLE) = 3
  lengths(IFLT) = 3
  lengths(IFNE) = 3
  lengths(IFNONNULL) = 3
  lengths(IFNULL) = 3
  lengths(IINC) = 3
  lengths(ILOAD) = 2
  lengths(IMUL) = 1
  lengths(INEG) = 1
  lengths(INSTANCEOF) = 3
  lengths(INVOKEDYNAMIC) = 5
  lengths(INVOKEINTERFACE) = 5
  lengths(INVOKESPECIAL) = 3
  lengths(INVOKESTATIC) = 3
  lengths(INVOKEVIRTUAL) = 3
  lengths(IOR) = 1
  lengths(IREM) = 1
  lengths(IRETURN) = 1
  lengths(ISHL) = 1
  lengths(ISHR) = 1
  lengths(ISTORE) = 2
  lengths(ISUB) = 1
  lengths(IUSHR) = 1
  lengths(IXOR) = 1
  lengths(L2D) = 1
  lengths(L2F) = 1
  lengths(L2I) = 1
  lengths(LADD) = 1
  lengths(LALOAD) = 1
  lengths(LAND) = 1
  lengths(LASTORE) = 1
  lengths(LCMP) = 1
  lengths(LCONST_0) = 1
  lengths(LCONST_1) = 1
  lengths(LDC) = 2
  lengths(LDIV) = 1
  lengths(LLOAD) = 2
  lengths(LMUL) = 1
  lengths(LNEG) = 1
  lengths(LOR) = 1
  lengths(LREM) = 1
  lengths(LRETURN) = 1
  lengths(LSHL) = 1
  lengths(LSHR) = 1
  lengths(LSTORE) = 2
  lengths(LSUB) = 1
  lengths(LUSHR) = 1
  lengths(LXOR) = 1
  lengths(MONITORENTER) = 1
  lengths(MONITOREXIT) = 1
  lengths(MULTIANEWARRAY) = 4
  lengths(NEW) = 3
  lengths(NEWARRAY) = 2
  lengths(NOP) = 1
  lengths(POP) = 1
  lengths(POP2) = 1
  lengths(PUTFIELD) = 3
  lengths(PUTSTATIC) = 3
  lengths(RET) = 2
  lengths(RETURN) = 1
  lengths(SALOAD) = 1
  lengths(SASTORE) = 1
  lengths(SIPUSH) = 3
  lengths(SWAP) = 2
  lengths(TABLESWITCH) = 4 // Variable, yet to see one in the RS code so I don't think it'll be a problem

  def toStart(insn: AbstractInsnNode, count: Int, name: String): Int = {
    Option(insn) match {
      case Some(i) =>
        if (i.getOpcode != -1) {
          i match {
            case v: VarInsnNode if v.`var` < 4 => toStart(i.getPrevious, count + 1, name)
            case l: LdcInsnNode =>
              if (Updater.classReaders(name).isWideConstant(l))
                toStart(i.getPrevious, count + 3, name)
              else
                toStart(i.getPrevious, count + 2, name)
            case _=> toStart(i.getPrevious, count + lengths(i.getOpcode), name)
          }
        } else
          toStart(i.getPrevious, count, name)
      case _ => count
    }
  }

  def getInstructionByteIndex(insn: AbstractInsnNode, cname: String): Int = {
    // Count the number of bytes between the this instruction and the beginning of the method
    toStart(insn.getPrevious, 0, cname)
  }
}
